
from . import jalali
from django.utils import timezone
def converter_number(num):
    con={'0':'۰','1':'۱','2':'۲','3':'۳','4':'۴','5':'۵',"6":'۶',"7":'۷',"8":'۸','9':'۹'}
    for k ,v in con.items():
        num=num.replace(k,v)
    return num


def jalali_con(time):
    time=timezone.localtime(time)
    famonth=[
 'فروردین'
,	'اردیبهشت'
,	'خرداد'
,	'تیر'
,	'مرداد'
,	'شهریور'
,	'مهر'
,	'آبان'
,	'آذر'
,	'دی'
,	'بهمن',
'اسفند'
    ]
    timeE=f'{time.year},{time.month},{time.day}'
    khro=jalali.Gregorian(timeE).persian_tuple()
    khro=f'{khro[2]} {famonth[khro[1]-1]} {khro[0]} ساعت {time.hour}:{time.minute} '
    return converter_number(khro)