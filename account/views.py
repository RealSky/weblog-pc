from django.shortcuts import render
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from blog.models import Post
# Create your views here.
# @login_required
# def Home(req):
#     return render(req,'registration/home.html')
class AcViews(LoginRequiredMixin,ListView):
    template_name='registration/home.html'
    queryset=Post.objects.all()
    context_object_name='data'