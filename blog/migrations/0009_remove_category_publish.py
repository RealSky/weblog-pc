# Generated by Django 3.0.14 on 2021-08-03 07:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_category_created'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='publish',
        ),
    ]
