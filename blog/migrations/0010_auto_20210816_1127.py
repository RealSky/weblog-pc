# Generated by Django 3.0.14 on 2021-08-16 06:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_remove_category_publish'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='parent',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='blog.Category', verbose_name='sub cat'),
        ),
        migrations.AlterField(
            model_name='post',
            name='cat',
            field=models.ManyToManyField(related_name='cat', to='blog.Category', verbose_name='دسته بندی'),
        ),
    ]
