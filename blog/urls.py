from django.db.models.fields import related
from django.urls import path,re_path
from django.conf import settings
from .views import  pashm,LiView,DetailPost,CategorList,AuthorList
app_name='blog'
urlpatterns = [
    path('av/',pashm,name='av')
    ,path('',LiView.as_view(),name='data'),
    # path('home/page/<int:page>/',baz,name='data'),
    path('home/page/<int:page>/',LiView.as_view(),name='data'),
    # re_path(r'(?P<slug>[-\w]+)-(?P<status>[-\w]+)',linkmojjazza,name='linkmojjazza')
    path('as/<slug:slug>/',DetailPost.as_view(),name='linkmojjazza'),
    # path('category/<slug:slug>/',namayeshCate,name='namayeshCate'),
    # path('category/<slug:slug>/page/<int:page>/',namayeshCate,name='namayeshCate')
    path('category/<slug:slug>/',CategorList.as_view(),name='namayeshCate'),
    path('category/<slug:slug>/page/<int:page>/',CategorList.as_view(),name='namayeshCate'),
    ########
    path('Author/<slug:UserName>',AuthorList.as_view(),name='authorU'),
    path('Author/<slug:UserName>/page/<int:page>',AuthorList.as_view(),name='authorU'),
]
