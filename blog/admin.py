from blog.models import Post,Category
from django.contrib import admin
from extentions.utils import converter_number
# Register your models here.
# Actions Post
def publishkardar(modeladmin,request,queryset):
    beach=queryset.update(status='publish')
    if beach == 1:
        msa=' یک پست منتشر شد'
    else:
        msa=f'{beach} پست منتشر شدند'
    modeladmin.message_user(request,msa)
publishkardar.short_description='تغییر دهنده به پابلیش'

def draftkardar(modeladmin,request,queryset):
    beach2=queryset.update(status='draft')
    if beach2 == 1:
        msv='یک پست پیش نویس شد'
    else:
        msv=f'{beach2} پست پیش نویس شدند'
    modeladmin.message_user(request,msv)
draftkardar.short_description='تغییر دهنده به درفت'
# namyesh moshakhsate Post

class PostAdmin(admin.ModelAdmin):
    list_display=("title",'author','PictView',"sluglink",'jlcreat','status',"jalalpub",'cat_conv')
    list_filter=("status",'publish')
    search_fields=("title",'body')
    prepopulated_fields={'sluglink':('title',)}
    list_display_links=('title',)
    # list_editable=('status',)
    date_hierarchy='publish'
    # raw_id_fields=('author',)
    ordering=('-created',)
    actions=[publishkardar,draftkardar]
    def cat_conv(self,objct):
        return '، '.join([i.title for i in objct.cat.CategoryActive()])
    cat_conv.short_description='دسته بندی'    
admin.site.register(Post,PostAdmin)

# Actions Categorys
def NamyeshActive(mdladmin,request,queryset):
    MSg=queryset.update(status=True)
    if MSg == 1:
        message_us=' دسته بندی نمایش داده شد!'
    else:
        message_us=' دسته بندی نمایش داده شدند'
    mdladmin.message_user(request,f'{MSg}{message_us}')
NamyeshActive.short_description='نمایش داده شوند؟'    

def NamyeshDeActive(mdladmin,request,queryset):
    Mizan=queryset.update(status=False)
    if Mizan == 1:
        Massage = f'{1}دسته بندی تغییر پیدا کرد'
    else:
        Massage=f'{converter_number(str(Mizan))} دسته بندی تغییر پیدا کردند'
    mdladmin.message_user(request,Massage)

NamyeshDeActive.short_description='نمایش داده نشوند؟'    
# namayesh moshakasate Categorys
class CatAdmin(admin.ModelAdmin):
    list_display=('title','position','sluglink','status','jlcreat','parent')
    list_filter=('position','sluglink')
    search_fields=("title",'sluglink')
    prepopulated_fields={'sluglink':('title',)}
    actions=[NamyeshActive,NamyeshDeActive]
    
admin.site.register(Category,CatAdmin)