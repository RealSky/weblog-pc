from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.html import format_html
from extentions.utils import jalali_con
#manager
class PostManager(models.Manager):
    def pubadmin(self):
        return self.filter(status='publish')
class CatManager(models.Manager):
    def CategoryActive(self):
        return self.filter(status=True)

# Create your models here.

class Category(models.Model):
    title=models.CharField(max_length=256,verbose_name='عنوان')
    sluglink=models.SlugField(unique=True,max_length=22,verbose_name='آدرس دسته بندی')
    parent=models.ForeignKey('self',default=None,null=True,blank=True,on_delete=models.SET_NULL,related_name='children',verbose_name='زیر دسته')
    status=models.BooleanField(default=True,verbose_name='دیده شدن')
    position=models.IntegerField(verbose_name='پوزیشن')
    # publish=models.DateTimeField(default=timezone.now,verbose_name='زمان ارائه')    
    created=models.DateTimeField(auto_now_add=True,null=True)
    class Meta:
        verbose_name='دسته بندی'
        verbose_name_plural=' دسته بندی ها'
        ordering=('parent_id',)
    def __str__(self) -> str:
        return self.title
    def jlcreat(self):
        return jalali_con(self.created)
    jlcreat.short_description='زمان ساخت'
    objects=CatManager()
    
class Post(models.Model):
    verbose_name='یییی'
    STATUS_CHOICES=(

        ('publish','منتشر شده'),
        ('draft','آماده انتشار')
    )
    title=models.CharField(max_length=256,verbose_name='عنوان')
    sluglink=models.SlugField(unique_for_date='publish',max_length=22,verbose_name='آدرس مطلب موردنظر')
    author=models.ForeignKey(User,null=True,on_delete=models.SET_NULL,related_name="authorM",default='انتخاب کنید',verbose_name='نویسنده')
    body=models.TextField(verbose_name='محتوا')
    publish=models.DateTimeField(default=timezone.now,verbose_name='زمان ارائه')    
    created=models.DateTimeField(auto_now_add=True)
    cat=models.ManyToManyField(Category,verbose_name='دسته بندی',related_name='cat')
    updated=models.DateTimeField(auto_now=True)
    status=models.CharField(choices=STATUS_CHOICES,max_length=256,verbose_name='وضعیت',default='')
    picture=models.ImageField(upload_to='images',verbose_name='تصویر')
    class Meta:
        ordering=("-publish",)
        verbose_name='پست'
        verbose_name_plural='پست'
    def __str__(self) -> str:
        return self.title
    def jalalpub(self):
        return jalali_con(self.publish)
    jalalpub.short_description='زمان انتشار'
    def jlcreat(self):
        return jalali_con(self.created)
    jlcreat.short_description='زمان ساخت'
    objects=PostManager()
    def PictView(self):
        return format_html(f'<img width=100 src="{self.picture.url}">')